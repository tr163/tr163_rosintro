#!/usr/bin/bash

rosservice call /turtle1/set_pen "255" "0" "0" "2" "0"

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[-1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[-1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -1.57]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'


#Turtle 2

rosservice call /spawn "7.8" "3.0" "0.0" "turtle2"

rosservice call /turtle2/set_pen "0" "255" "0" "2" "0"

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
  -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.57]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
  -- '[2.3, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
  -- '[-0.54, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
  -- '[1.8 , 0.0 , 0.0]' '[0.0 , 0.0 , -2.8]'