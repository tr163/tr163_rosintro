% Load the UR5e robot model
ur5e = loadrobot('universalUR5e');

% Generate a random joint configuration
randomJointConfig = randomConfiguration(ur5e);

% Change the joint positions to specific values
specificJointPositions = [-0.2699918725279984, -1.4828961698284466, 1.9838082248922504, 4.219863608525713, -4.712658717696234, 4.9821070540622525]

specificJointPositions2 = [0.3737216590465451, 0.3744391431641576, -1.9762302379543604, 0.03617262130292609, 1.5708303997278215, -1.9444345040076314]
 
specificJointPositions3 = [-0.7296642319166793, -0.22152205075766584, -2.5485556666462115, 1.196862498149632, 1.5708289655745835, -0.8412919951540871]

for i = 1:length(randomJointConfig)
    randomJointConfig(i).JointPosition = specificJointPositions3(i);
end

% Display the modified joint configuration in detail
disp('Modified Joint Configuration:');
for i = 1:length(randomJointConfig)
    disp(['Joint ' num2str(i) ' - Name: ' randomJointConfig(i).JointName ', Position: ' num2str(randomJointConfig(i).JointPosition)]);
end

% Calculate the Jacobian matrix
Jacobian = geometricJacobian(ur5e, randomJointConfig, 'flange');

% Display the Jacobian matrix
disp('Jacobian Matrix:');
disp(Jacobian);
