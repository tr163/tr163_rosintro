% Define symbolic joint variables
syms q1 q2 q3 q4 q5 q6 real
syms d1 d4 d5 d6
syms a2 a3
% Define Denavit-Hartenberg parameters for UR5e
a = [0, a2, a3, 0, 0, 0];
d = [d1, 0, 0, d4, d5, d6];
alpha = [-90, 0, 0, -90, 90, 0];

d1 = 0.1625
d4 = 0.1333
d5 = 0.997
d6 = 0.0966

a2 = -0.425
a3 = -0.3922
% Compute transformation matrices
T = eye(4);
for i = 1:6
    A = [cos(eval(['q', num2str(i)])), -sin(eval(['q', num2str(i)]))*cosd(alpha(i)), sin(eval(['q', num2str(i)]))*sind(alpha(i)), a(i)*cos(eval(['q', num2str(i)]));
         sin(eval(['q', num2str(i)])), cos(eval(['q', num2str(i)]))*cosd(alpha(i)), -cos(eval(['q', num2str(i)]))*sind(alpha(i)), a(i)*sin(eval(['q', num2str(i)]));
         0, sind(alpha(i)), cosd(alpha(i)), d(i);
         0, 0, 0, 1];
    T = T * A;
end


% Display symbolic T matrix
disp('Symbolic T Matrix:');
disp(T);


% Specific joint positions
specificJointPositions = [-0.2699918725279984, -1.4828961698284466, 1.9838082248922504, 4.219863608525713, -4.712658717696234, 4.9821070540622525];
specificJointPositions2 = [0.3737216590465451, 0.3744391431641576, -1.9762302379543604, 0.03617262130292609, 1.5708303997278215, -1.9444345040076314];
specificJointPositions3 = [-0.7296642319166793, -0.22152205075766584, -2.5485556666462115, 1.196862498149632, 1.5708289655745835, -0.8412919951540871];

% Compute transformation matrices for specific joint positions
T1 = subs(T, [q1, q2, q3, q4, q5, q6], [specificJointPositions]);
T2 = subs(T, [q1, q2, q3, q4, q5, q6], [specificJointPositions2]);
T3 = subs(T, [q1, q2, q3, q4, q5, q6], [specificJointPositions3]);

% Display the transformation matrices
disp('Transformation Matrix for Specific Joint Positions 1:');
disp(eval(T1));
disp('Transformation Matrix for Specific Joint Positions 2:');
disp(eval(T2));
disp('Transformation Matrix for Specific Joint Positions 3:');
disp(eval(T3));
