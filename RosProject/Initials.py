#!/usr/bin/env python3

import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import copy
import sys

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))

#roslaunch ur_gazebo ur5e_bringup.launch
#roslaunch ur5e_moveit_config moveit_planning_execution.launch sim:=true
#roslaunch ur5e_moveit_config moveit_rviz.launch
#rosrun ur5e_moveit_config initial.py

class RobotController(object):
    def __init__(self):
        super(RobotController, self).__init__()

        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("moveit_commander", anonymous=True)

        ## Instantiate a `RobotCommander`_ object.
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)


        ## Create a `DisplayTrajectory`_ ROS publisher.
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        #Self init
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher

    def go_to_pose_goal(self, pose_goal):
        move_group = self.move_group

        move_group.set_pose_target(pose_goal)

        # `go()` returns a boolean indicating whether the planning and execution was successful.
        success = move_group.go(wait=True)

        # Calling `stop()` ensures that there is no residual movement
        move_group.stop()

        #Clear your targets after planning with poses.
        move_group.clear_pose_targets()

        current_pose = self.move_group.get_current_pose().pose
        
        return success, current_pose
    
    def plan_cartesian_path(self, waypoints):
        move_group = self.move_group

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this tutorial.
        (plan, fraction) = self.move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )
        return plan, fraction

    def display_trajectory(self, plan):
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher

        ## A `DisplayTrajectory`_ msg has two primary fields, trajectory_start and trajectory.
        ## We populate the trajectory_start with our current robot state to copy over
        ## any AttachedCollisionObjects and add our plan to the trajectory.
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)

        # Publish
        display_trajectory_publisher.publish(display_trajectory)

def main():
    robot_controller = RobotController()


    def go_to_starting_position():
        #Moves the robot to the necessary starting position for all 
        #future behavior to be consistent.
        start_pose = geometry_msgs.msg.Pose()
        start_pose.position.x = 0.5
        start_pose.position.y = 0.0
        start_pose.position.z = 0.5
        success, current_pose = robot_controller.go_to_pose_goal(start_pose)
        if not success:
            rospy.logerr("Failed to reach initial pose")
            return

        # Get current joint values
        joint_angles = robot_controller.move_group.get_current_joint_values()
    
        # Print joint angles
        print("Joint Angles at Starting Position:")
        print(joint_angles)
        print("Starting Position Reached")

    def drawT_cartesianPath():
        #Draws the letter T
        print("Starting Letter T")

        t_waypoints = [
            geometry_msgs.msg.Pose(),
            geometry_msgs.msg.Pose(),
            geometry_msgs.msg.Pose(),
            geometry_msgs.msg.Pose()
        ]

        #Left of bottom part of letter T
        t_waypoints[0].position.x = 0.5
        t_waypoints[0].position.y = 0.0
        t_waypoints[0].position.z = 0.5

        #Right of top part of letter T
        t_waypoints[1].position.x = 0.3
        t_waypoints[1].position.y = 0.0
        t_waypoints[1].position.z = 0.5

        #Start of the vertical top of letter T
        t_waypoints[2].position.x = 0.4
        t_waypoints[2].position.y = 0.0
        t_waypoints[2].position.z = 0.5

        #Bottom of vertical part of letter T
        t_waypoints[3].position.x = 0.4
        t_waypoints[3].position.y = 0.3
        t_waypoints[3].position.z = 0.5

        # Plan and execute the Cartesian path
        cartesian_plan, fraction = robot_controller.plan_cartesian_path(t_waypoints)
        robot_controller.display_trajectory(cartesian_plan)
        robot_controller.move_group.execute(cartesian_plan, wait=True)

        # Get current joint values
        joint_angles = robot_controller.move_group.get_current_joint_values()
    
        # Print joint angles
        print("Joint Angles at Starting Position:")
        print(joint_angles)

    def TtoR():
        #Moves from the end of letter T to the beginning of letter R
        waypoint = [geometry_msgs.msg.Pose()]

        waypoint[0].position.x = 0.2
        waypoint[0].position.y = 0.0
        waypoint[0].position.z = 0.5

        # Plan and execute the Cartesian path
        cartesian_plan, fraction = robot_controller.plan_cartesian_path(waypoint)
        robot_controller.display_trajectory(cartesian_plan)
        robot_controller.move_group.execute(cartesian_plan, wait=True)

        # Get current joint values
        joint_angles = robot_controller.move_group.get_current_joint_values()
    
        # Print joint angles
        print("Joint Angles at Starting Position:")
        print(joint_angles)

    def drawR_cartesianPath():
        #Draws the letter R
        r_waypoints = [
            geometry_msgs.msg.Pose(),
            geometry_msgs.msg.Pose(),
            geometry_msgs.msg.Pose(),
            geometry_msgs.msg.Pose(),
            geometry_msgs.msg.Pose(),
            geometry_msgs.msg.Pose()
        ]

        print("Starting Letter R")

        r_waypoints[0].position.x = 0.2
        r_waypoints[0].position.y = 0.3
        r_waypoints[0].position.z = 0.5

        # Diagonal line of letter R
        r_waypoints[1].position.x = 0.2
        r_waypoints[1].position.y = 0.2
        r_waypoints[1].position.z = 0.5

        # Horizontal line of letter R
        r_waypoints[2].position.x = 0.1
        r_waypoints[2].position.y = 0.1
        r_waypoints[2].position.z = 0.5

        # Return to starting position
        r_waypoints[3].position.x = 0.2
        r_waypoints[3].position.y = 0.0
        r_waypoints[3].position.z = 0.5

        # Return to starting position
        r_waypoints[4].position.x = 0.2
        r_waypoints[4].position.y = 0.2
        r_waypoints[4].position.z = 0.5

        #Return to starting position
        r_waypoints[5].position.x = 0.1 
        r_waypoints[5].position.y = 0.3
        r_waypoints[5].position.z = 0.5

        # Plan and execute the Cartesian path
        cartesian_plan, fraction = robot_controller.plan_cartesian_path(r_waypoints)
        robot_controller.display_trajectory(cartesian_plan)
        robot_controller.move_group.execute(cartesian_plan, wait=True)

    # Move the robot to the starting position
    go_to_starting_position()

    #Draw the letter T using the plan_cartesian_path function
    drawT_cartesianPath()

    #Move from end of T to beginning of R
    TtoR()

    #Draw letter R
    drawR_cartesianPath()

    #Done
    print("Done :0")


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
